using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class enemigo : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] float chaseDistance = 5f;

    private bool isProvoked = false;

    private GameObject player;

    float Distance;

    NavMeshAgent navMeshAgent;



    private void Awake()

    {

        navMeshAgent = GetComponent<NavMeshAgent>();

        player = GameObject.FindGameObjectWithTag("Player");

    }

     // Update is called once per frame

    void Update()

    {

        if (CheckDistance())

        {

            isProvoked = true;

            Debug.Log("Atacar");

            ChaseTarget();



        }

        else

        {

            Debug.Log("regresar al punto cuidando");

        }

    }



    private bool CheckDistance()

    {

        Distance = Vector3.Distance(gameObject.transform.position, player.transform.position);

        return (Vector3.Distance(gameObject.transform.position, player.transform.position) <= chaseDistance);

    }



    private void ChaseTarget()

    {

        navMeshAgent.SetDestination(player.transform.position);

    }



    private void OnDrawGizmosSelected()

    {

        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(gameObject.transform.position, chaseDistance);

    }


}
